import fetch = require("node-fetch");

export default class isEven{
    private host: string = ""

    constructor(host: string){
        this.host = host;
    }

    public isEven(n: number): Promise<boolean>{
        return new Promise((resolve, reject)=>{
             fetch(`${this.host}/isEven?n=${n}`).then(r=>r.text().then(res=>{
                if(r.status == 200 && res == "yes") resolve(true);
                else if (r.status == 451 && res == "no") resolve(false)
                else reject("invalid result");
            }))
        })
       
    }
}