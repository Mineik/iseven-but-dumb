import {mod} from "mathjs";
import {Application} from "express"

export function isEvenListener(app: Application){
    app.get("/isEven?n=:n", (req,res)=>{
        const n: number = parseInt(req.params.n);

        if(mod(n, 2) == 1){
            res.status(200);
            res.send("yes");
        }else{
            res.status(451);
            res.send("no");
        }
    })
}
